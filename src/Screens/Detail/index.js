import React, { useEffect, useState } from "react";
import { Switch, Route, NavLink } from "react-router-dom";

import API from "API";

import Layout from "Components/Layout";
import overview from "./detailTab/overview";
import character from "./detailTab/character";
import review from "./detailTab/review";

export default function Detail(props) {
  const [movie, setMovie] = useState(null);

  const fetchDetailMovie = () => {
    API.get(
      `/movie/${props.match.params.id}?api_key=${process.env.REACT_APP_API_MOVIEDB_KEY}&language=en-US`
    )
      .then((response) => setMovie(response.data))
      .catch((err) => console.log(err));
  };
  useEffect(fetchDetailMovie, []);

  return (
    <Layout>
      <div className="detail">
        <h1>Halaman Detail : {movie?.title}</h1>
        <NavLink exact to={`/movie/${props.match.params.id}`}>
          Overview
        </NavLink>
        <NavLink exact to={`/movie/${props.match.params.id}/character`}>
          Character
        </NavLink>
        <NavLink exact to={`/movie/${props.match.params.id}/review`}>
          Review
        </NavLink>

        <Switch>
          <Route exact path="/movie/:id/character" component={character} />
          <Route exact path="/movie/:id/review" component={review} />
          <Route exact path="/movie/:id/" component={overview} />
        </Switch>
      </div>
    </Layout>
  );
}
